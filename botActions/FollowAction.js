const Subscribe = require("../models/subscribe")

const followAction  = async (ctx) => {
    let coinsOfUser = [];
    const nameCoin = ctx.update.callback_query.message.text.split("\n")[0].slice(7)
    console.log("name follow:" + nameCoin);
    await Subscribe.find({
      user_id: ctx.update.callback_query.message.chat.id
    }).then(res => res.forEach(i => coinsOfUser.push(i.coin_title)))
    console.log("coins of user" + coinsOfUser)
    if(coinsOfUser.indexOf(nameCoin) === -1){
      let newSubscribe = new Subscribe({
        user_id: ctx.update.callback_query.from.id,
        coin_title: nameCoin
      })
      newSubscribe.save()
      ctx.telegram.sendMessage(ctx.update.callback_query.from.id, `Теперь вы следите за токеном ${nameCoin}`)
    }
    else {
      ctx.telegram.sendMessage(ctx.update.callback_query.from.id, `Вы уже следите за токеном ${nameCoin}`)
    }
  }
module.exports = followAction