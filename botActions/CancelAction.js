const Subscribe = require("../models/subscribe");

const cancelAction = async (ctx) => {
    let coinsOfUser = [];
    const nameCoin = ctx.update.callback_query.message.text.split("\n")[0].slice(7)
    console.log("name follow:" + nameCoin);
    await Subscribe.find({
      user_id: ctx.update.callback_query.message.chat.id
    }).then(res => res.forEach(i => coinsOfUser.push(i.coin_title)));
    console.log("coins of user" + coinsOfUser)
    if(coinsOfUser.indexOf(nameCoin) !== -1){
      await Subscribe.deleteOne({
        user_id: ctx.update.callback_query.message.chat.id,
        coin_title: nameCoin
      }
      )
      ctx.telegram.sendMessage(ctx.update.callback_query.from.id, `Вы больше не следите за токеном ${nameCoin} `)
    }
  }
  module.exports = cancelAction

