let { Schema, model } = require('mongoose')
let userSchema = new Schema({
    _id: Number,
    username: String,
    firstName: String,
    lastName: String,
    phone: String,
    langCode: String,
    chatID: Number
}, {
    _id: false
})

let User = model('users', userSchema);
module.exports = User