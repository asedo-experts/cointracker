const got = require("got").default;
const jsdom = require("jsdom");
const {JSDOM} = jsdom;
const CancelFollow = require("./inline_keyboards/cancelFollow");
const Follow = require("./inline_keyboards/follow")
const mongoose = require("mongoose");
const LastCoin = require("./models/lastCoin");
const {Telegraf} = require("telegraf");
const User = require("./models/user");
var cron = require("node-cron");
const Subscribe = require('./models/subscribe')
const followAction = require("./botActions/FollowAction")
const cancelAction = require("./botActions/CancelAction")

const bot = new Telegraf("1886772671:AAGKdLqJ-MDTT649K0fbru2x0mHBQw5hPUc");
mongoose.connect(
  "mongodb+srv://pankiblack:Paninm27@cluster0.i81rv.mongodb.net/test-coin-tracker?retryWrites=true&w=majority"
);

bot.start(async (ctx) => {
  ctx.reply(`Hello ${ctx.update.message.from.first_name}`);
  let user = await User.findOne({
    chat_id: ctx.update.message.chat.id
  });
  if (user === null) {
    let newUser = new User({
      _id: ctx.update.message.from.id,
      username: ctx.update.message.from.username,
      firstName: ctx.update.message.from.first_name,
      lastName: ctx.update.message.from.last_name,
      phone: ctx.update.message.from.phone,
      langCode: ctx.update.message.from.language_code,
      chatID: ctx.update.message.chat.id,
    });
    await newUser.save();
  }
});

fetch();

 cron.schedule("* * * * *", () => {
   console.log("Fetching...");
   fetch();
 }
 );

let coinsUser = [];
async function fetch() {
  await got("https://coinmarketcap.com/new/").then(async (response) => {
    const dom = await new JSDOM(response.body);
    let coins = [];
    await dom.window.document.querySelectorAll("tr").forEach(async (element, i) => {
      if (i === 0) {
        return true;
      }
      let title = element.children.item(2).firstChild.firstChild.lastChild
        .firstChild.textContent;
      let link =
        "https://coinmarketcap.com" + element.children.item(2).firstChild.href;
      let price = element.children.item(3).textContent;
      let volume = element.children.item(7).textContent;
      let blockchain = element.children.item(8).textContent;
      let date = element.children.item(9).textContent;
      let changePrice = element.children.item(4).firstChild.firstChild.classList.contains("icon-Caret-down") ? `update price - ${element.children.item(4).firstChild.textContent}` : `update price + ${element.children.item(4).firstChild.textContent}`

      if (title === "") {
        return false;
      }

      let coinsObject = {
        title,
        price,
        volume,
        blockchain,
        date,
        link,
        changePrice
      };
      coins.push(coinsObject)
    });

    print(coins);
    coinsUser = coins.slice()
  });
}

async function print(arr = []) {
  let arrNewCoins = [];
  try {
    User.find({}).then(async (users) => {
      LastCoin.findOne({})
        .sort({
          _id: -1
        })
        .then((last) => {
          for (let i = 0; i < arr.length; i++) {
            if (last.title === arr[i].title) {
              break;
            } else {
              arrNewCoins.push(arr[i])
            }
          }
          if (arrNewCoins) {
            arrNewCoins.reverse().forEach((elem => {
              let newLastCoin = new LastCoin({
                title: elem.title,
              });
              newLastCoin.save();
              users.forEach((user) => {
                bot.telegram.sendMessage(
                  user.chatID,
                  `Title: ${elem.title}\nPrice: ${elem.price}\nVolume: ${elem.volume}\nBlockcahin: ${elem.blockchain}\nTime: 
                ${elem.date}\nLink: ${elem.link}`, Follow

                )
              })
            }))
          }
          throw BreakException;

        })
    })
  } catch (e) {
    if (e !== BreakException) throw e;
  }
}


bot.action("Follow", followAction)
bot.action("Cancel", cancelAction)

bot.command("/update", async  (ctx) => {
  bot.telegram.sendMessage(ctx.update.message.chat.id, "Теперь вы следите за обновлениями выбранных токенов")
  cron.schedule("*/5 * * * *", () => {
    try{
      let user;
      let arrTitle = [];
      Subscribe.find({
        user_id: ctx.update.message.chat.id
      }).then(async (last) => {
        user = last[0].user_id;
        await last.forEach(i => {
          arrTitle.push(i.coin_title)
        })
        await arrTitle.forEach(item => {
          coinsUser.forEach(elem => {
            if (item === elem.title) {
              ctx.telegram.sendMessage(user, `Title: ${elem.title}\nPrice: ${elem.price}\nPriceChanges: ${elem.changePrice}\nVolume: ${elem.volume}\nBlockcahin: ${elem.blockchain}\nLink: ${elem.link}`, CancelFollow)
          }
        })
      })
    })
    }
    catch (e) {
      if (e !== BreakException) throw e;
    }
  }); 
})

bot.launch();